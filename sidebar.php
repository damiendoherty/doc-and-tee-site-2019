
<?php if ( is_dynamic_sidebar( 'sidebar-primary' ) ) : ?>
<div id="sidebar" class="page-sidebar" role="complementary">
	<?php dynamic_sidebar('sidebar-primary'); ?>
</div>
<?php endif; ?>
