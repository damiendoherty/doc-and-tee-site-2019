<?php
use Roots\Sage\Setup;
use Roots\Sage\Wrapper;
use Roots\Sage\Extras;
?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
    <?php get_template_part('head'); ?>
	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage" data-toggler=".show-nav" id="bodynav">
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    
		<div class="off-canvas-wrapper">


			<div class="off-canvas-content <?php if (!has_post_thumbnail( $post->ID ) ){echo 'no-thumb-here';} ?>" data-off-canvas-content>

				<?php get_template_part('templates/headers/header-mobile'); ?>


				<div class="wrap container <?php echo get_post_type() . '-container' . ( ( is_singular( 'post' ) ) ? ' infinitescroll' : '' ); ?>" role="document">
					<main class="main<?php echo ( is_singular( 'post' ) ) ? ' append' : ''; ?>">
						<?php include Wrapper\template_path(); ?>
					</main><!-- /.main -->
				</div><!-- /.wrap -->
				
				<?php if ( is_singular( 'post' ) ) : ?>
				<div class="row column text-center" data-magellan>
					<a href="#post-<?php the_ID(); ?>" class="button">Back to Top</a>
				</div>
				<?php endif; ?>
				
				<?php do_action('get_footer'); get_template_part('footer'); wp_footer(); ?>

			</div><!-- .off-canvas-content -->


		</div><!-- .off-canvas-wrapper -->
			<script>
				( function ( $ ) { $( document ).ready( function() {

					$( '.hamburger' ).on( 'click', function() {
						$( this ).toggleClass( 'is-active' );
					} );

				} ); } )( jQuery );
			</script>
	</body>
</html>