.mobile-header {
	background: transparent;
    position: absolute;
    width: 100%;
    border-bottom: none;
    z-index: 999;
	
	.header-wrap {
		display: flex;
		align-items: center;
		justify-content: space-between;
        transition: all .75s ease;
		
		.main-logo {
			padding: .5rem 1rem;
            
            @include breakpoint(medium) {
                padding: 1rem;
            }
            
            .custom-logo-link {
                position: relative;
                display: flex;
                align-items: center;
                justify-content: center;
                width: 46px;
                height: 46px;
                
                @include breakpoint(medium) {
                    width: 60px;
                    height: 60px;
                }
                
                &:hover {
                    .svg-logo {
                        @include jello;
                    }
                }
                
                .svg-logo {
                    width: 46px;
                    height: 46px;
                    position: absolute;
                    transition: all .5s ease;
                    
                    @include breakpoint(medium) {
                        width: 60px;
                        height: 60px;
                    }
                    
                    &.white {
                        opacity: 1;
                    }
                    
                    &.blue {
                        opacity: 0;
                    }
                }
            }
		}
	}
	
	.hamburger {
		display: flex;
    	align-items: center;
		padding: .5rem 1rem;
		cursor: pointer;
        
        @include breakpoint(medium) {
            padding: 1rem;
        }
		
		&:hover, &:active, &:focus {
			background: transparent;
		}
		
		.hamburger-box {
			
			.hamburger-inner {
				background-color: #fff;
    			height: 2px;
				
				&:after, &:before {
					background-color: #fff;
    				height: 2px;
				}
				
			}
			
		}
		
		.hamburger-label {
			color: $body-font-color;
			padding: 0 .5rem;
			font-size: 1.2rem;
		}
	}
    
    .sticky.is-stuck {

        .hamburger .hamburger-box .hamburger-inner {
            background-color: $brand-primary;

            &:after, &:before {
                background-color: $brand-primary;
            }
        }
        
        .main-logo .svg-logo {
            &.white {
                opacity: 0;
            }

            &.blue {
                opacity: 1;
            }
        }
    }
}

.no-thumb-here,
.archive,
.show-nav,
.home {
    .mobile-header .header-wrap .main-logo .svg-logo {
        &.white {
            opacity: 0;
        }

        &.blue {
            opacity: 1;
        }
    }
    
    .mobile-header .hamburger .hamburger-box .hamburger-inner {
        background-color: $brand-primary;

        &:after, &:before {
            background-color: $brand-primary;
        }
    }
}


.mobile-menu-wrapper {
	z-index: -1;
	opacity: 0;
	width: 100%;
    position: fixed;
    top: 0;
    overflow: auto;
    left: 0;
    height: 100vh;
	transition: all .5s ease;
	background: rgba($brand-secondary,.95);
	display: flex;
    align-items: center;
    justify-content: center;
	
	.mob-inner {
		width: 100%;
	}
}

body.show-nav {
	overflow: hidden;
	
	.mobile-menu-wrapper {
		z-index: 998;
		opacity: 1;
		
		.mobile-menu {
			@include bounceInLeft(
				$duration: 1s,
				$delay: .1s
			);
		}
	}
}


.mobile-menu {

	.vertical.menu {
		& > .menu-item {
			
			& > a {
				@include breakpoint(medium) {
					font-size: 1.4rem;
				}

				&:hover {
					color: $brand-primary;
					text-decoration: underline;
				}
			}

			& > .menu {
				margin-left: 1rem;
				
				& > li a {
					padding: .25rem 1rem;
					
					&:hover {
						color: $brand-primary;
					}
				}
			}

			.submenu-toggle {
				border: 1px solid $brand-primary;
				background: transparent;
				width: 30px;
				height: 30px;
				padding: 0;

				&:after {
					border-color: $brand-primary transparent transparent;
				}
			}
		}
	}
}
