( function ( $ ) {
	
	$( document ).ready( function() {
		
		$( document ).foundation();
		new WOW().init();
		
		$('#mobmen').css('paddingTop', $('#HeaderMobile').outerHeight()+'px');
		
	} );
	
	$( '.infinitescroll' ).infiniteScroll( {
		path: ( $( 'body' ).hasClass( 'single' ) ) ? function(){ return $( '.post-navigation:last > .next-post > a' ).attr( 'href' ); } : '.posts-pagination > .navigation > ul > li.next-post > a',
		append: '.append',
		scrollThreshold: 650,
		history: 'push',
		historyTitle: true,
		hideNav: false,
		debug: false
	} );
	
} )( jQuery );
