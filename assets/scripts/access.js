( function ( $ ) {
	
	$( document ).ready( function() {
		
		$( '[data-access="toggle"]' ).click( function( e ) {
			
			var controls = $( '#access-controls' );
			
			var is_open = controls.data( 'access' );
			is_open = ( 'open' == is_open ) ? true : false;
			
			if ( is_open ) {
				$( e.currentTarget ).toggleClass( 'isopen' );
				controls.stop().data( 'access', 'closed' ).slideUp( 'fast' );
			} else {
				$( e.currentTarget ).toggleClass( 'isopen' );
				controls.stop().data( 'access', 'open' ).slideDown( 'fast' );
			}
			
			return false;
			
		} );
		
		function getCookie(cname) {
			var name = cname + "=";
			var decodedCookie = decodeURIComponent(document.cookie);
			var ca = decodedCookie.split(';');
			for(var i = 0; i <ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) == ' ') {
					c = c.substring(1);
				}
				if (c.indexOf(name) == 0) {
					return c.substring(name.length, c.length);
				}
			}
			return "";
		}
		
		var theme_style = getCookie( 'theme-style' );
		if ( theme_style ) {
			$( 'body' ).addClass( 'theme-style-' + theme_style );
		}
		
		var theme_text_size = getCookie( 'theme-text-size' );
		if ( theme_text_size ) {
			$( 'body' ).addClass( 'theme-text-size-' + theme_text_size );
		}
		
		var theme_text_only = getCookie( 'theme-text-only' );
		if ( 'yes' == theme_text_only ) {
			$( 'body' ).addClass( 'theme-text-only' );
		}
		
	} );

} )( jQuery );