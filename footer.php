<footer id="footer-widgets" class="site-footer" itemscope itemtype="http://schema.org/WPFooter">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
		<?php dynamic_sidebar('sidebar-footer'); ?>
		</div>
		
		<div class="grid-x grid-margin-x cell accred align-center">
			&copy; <?php bloginfo( 'name' ); ?> <?php echo ( $year = date( 'Y' ) ) ? $year : ''; ?>
			<a class="doc" href="http://www.docandtee.com/" target="_blank" aria-label="Visit the website of Doc and Tee Ltd">Website by Doc&amp;Tee</a>
		</div>
	</div>

</footer>