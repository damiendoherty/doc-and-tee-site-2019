<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup() {
  // Enable features from Soil when plugin is activated
  // https://roots.io/plugins/soil/
    add_theme_support('soil-clean-up');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-relative-urls');

  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
    load_theme_textdomain('sage', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
    add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
    register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'sage'),
	'left_navigation' => __('Left Navigation', 'sage'),
	'right_navigation' => __('Right Navigation', 'sage')
    ]);

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
    add_theme_support('post-thumbnails');
	add_image_size( 'post-thumb-xsml', 370, 225, true );
	add_image_size( 'post-thumb-sml', 640, 390, true );
	add_image_size( 'post-thumb-med', 1024, 624, true );
	add_image_size( 'post-thumb-lrg', 1280, 780, true );
	add_image_size( 'post-thumb-xlrg', 1440, 878, true );
	add_image_size( 'post-thumb-xxlrg', 1920, 1170, true );
	add_image_size( 'post-thumb-sq', 640, 640, true );


  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
    add_theme_support('post-formats', ['gallery', 'video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
    add_editor_style(Assets\asset_path('styles/main.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Register sidebars
 */
function widgets_init() {
    register_sidebar(array(
    'name'          => __('Page sidebar', 'sage'),
    'id'            => 'sidebar-primary',
    'before_widget' => '<aside class="widget %1$s %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h1 class="widget-title">',
    'after_title'   => '</h1>'
    ));
	
	register_sidebar(array(
    'name'          => __('Footer', 'sage'),
    'id'            => 'sidebar-footer',
    'before_widget' => '<aside class="widget %1$s %2$s cell small-12 medium-6 large-4">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h1 class="widget-title">',
    'after_title'   => '</h1>'
    ));
	
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
    static $display;

    isset($display) || $display = !in_array(true, [
    // The sidebar will NOT be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    is_404(),
    is_front_page(),
    is_page_template('template-post-archive.php'),
    is_page_template('template-post-archive-filtered.php'),
    is_page_template('content-single-slider.php'),
    is_page_template('template-page-fullscreen-landing.php'),
    ]);

    return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets() {
    wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, null);

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    wp_enqueue_script('sage/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);

	wp_enqueue_style('post-production', get_stylesheet_directory_uri() . '/style.css');

}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);
