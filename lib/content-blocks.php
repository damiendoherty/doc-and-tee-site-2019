<?php

add_action('acf/init', 'my_acf_init');
function my_acf_init() {
	
	// check function exists
	if( function_exists('acf_register_block') ) {
		
		// register a block
		acf_register_block(array(
			'name'				=> 'content-block',
			'title'				=> __('Custom Content Block'),
			'description'		=> __('A custom text and image content block.'),
			'render_callback'	=> 'content_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'content-block'),
		));
        
        acf_register_block(array(
			'name'				=> 'slider-content-block',
			'title'				=> __('Custom image slider Content Block'),
			'description'		=> __('A custom text and image slider content block.'),
			'render_callback'	=> 'slider_content_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'content-block'),
		));
		
		acf_register_block(array(
			'name'				=> 'simple-content-block',
			'title'				=> __('Simple Custom Content Block'),
			'description'		=> __('A simple custom text and image content block.'),
			'render_callback'	=> 'simple_content_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'content-block'),
		));
        
        acf_register_block(array(
			'name'				=> 'hero-block',
			'title'				=> __('Custom Hero Block'),
			'description'		=> __('A custom text and image hero block.'),
			'render_callback'	=> 'hero_content_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'content-block'),
		));
        
        acf_register_block(array(
			'name'				=> 'hover-content-block',
			'title'				=> __('Custom Content Hover Block'),
			'description'		=> __('A custom text and image content block with text over the image.'),
			'render_callback'	=> 'hover_content_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'content-block'),
		));
        
        acf_register_block(array(
			'name'				=> 'team-content-block',
			'title'				=> __('Custom Team member block'),
			'description'		=> __('Add team members to your page.'),
			'render_callback'	=> 'team_content_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'content-block'),
		));
        
        acf_register_block(array(
			'name'				=> 'resource-content-block',
			'title'				=> __('Custom Resources block'),
			'description'		=> __('Add resources links and files to your page.'),
			'render_callback'	=> 'resource_content_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'content-block'),
		));
        
        acf_register_block(array(
			'name'				=> 'gallery-content-block',
			'title'				=> __('Custom Gallery block'),
			'description'		=> __('Add a custom gallery to your page.'),
			'render_callback'	=> 'gallery_content_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'content-block'),
		));
	}
}


function content_acf_block_render_callback() {
	global $post;
	
	if( get_field('full_width_blocks') == 'yes' ) {
		
		if( get_field('page_width', $post->ID) == 'sidebar' ) {
			get_template_part('templates/content-blocks/content-blocks');
		} else {
	
			echo '</div></div></div></div>';
				get_template_part('templates/content-blocks/content-blocks');
			echo '<div class="grid-container"><div class="grid-x grid-margin-x">';

			if( get_field('page_width', $post->ID) == 'narrow' ) {
				echo '<div class="cell small-12 medium-10 medium-offset-1 large-8 large-offset-2">';
			} elseif( get_field('page_width', $post->ID) == 'standard' ) {
				echo '<div class="cell small-12">';
			}

			echo '<div class="entry-content wow fadeIn" itemprop="articleBody" data-wow-duration="1s">';
		}
		
	} else {
		get_template_part('templates/content-blocks/content-blocks');
	}
}

function slider_content_acf_block_render_callback() {
	global $post;
	
	if( get_field('full_width_blocks') == 'yes' ) {
		
		if( get_field('page_width', $post->ID) == 'sidebar' ) {
			get_template_part('templates/content-blocks/slider-content-blocks');
		} else {
	
			echo '</div></div></div></div>';
				get_template_part('templates/content-blocks/slider-content-blocks');
			echo '<div class="grid-container"><div class="grid-x grid-margin-x">';

			if( get_field('page_width', $post->ID) == 'narrow' ) {
				echo '<div class="cell small-12 medium-10 medium-offset-1 large-8 large-offset-2">';
			} elseif( get_field('page_width', $post->ID) == 'standard' ) {
				echo '<div class="cell small-12">';
			}

			echo '<div class="entry-content wow fadeIn" itemprop="articleBody" data-wow-duration="1s">';
		}
		
	} else {
		get_template_part('templates/content-blocks/slider-content-blocks');
	}
}

function simple_content_acf_block_render_callback() {
	global $post;
	
	if( get_field('full_width_blocks') == 'yes' ) {
		
		if( get_field('page_width', $post->ID) == 'sidebar' ) {
			get_template_part('templates/content-blocks/simple-content-blocks');
		} else {
	
			echo '</div></div></div></div>';
				get_template_part('templates/content-blocks/simple-content-blocks');
			echo '<div class="grid-container"><div class="grid-x grid-margin-x">';

			if( get_field('page_width', $post->ID) == 'narrow' ) {
				echo '<div class="cell small-12 medium-10 medium-offset-1 large-8 large-offset-2">';
			} elseif( get_field('page_width', $post->ID) == 'standard' ) {
				echo '<div class="cell small-12">';
			}

			echo '<div class="entry-content wow fadeIn" itemprop="articleBody" data-wow-duration="1s">';
		}
		
	} else {
		get_template_part('templates/content-blocks/simple-content-blocks');
	}
}

function hero_content_acf_block_render_callback() {
	global $post;
	
	if( get_field('full_width_blocks') == 'yes' ) {
		
		if( get_field('page_width', $post->ID) == 'sidebar' ) {
			get_template_part('templates/content-blocks/hero-blocks');
		} else {
	
			echo '</div></div></div></div>';
				get_template_part('templates/content-blocks/hero-blocks');
			echo '<div class="grid-container"><div class="grid-x grid-margin-x">';

			if( get_field('page_width', $post->ID) == 'narrow' ) {
				echo '<div class="cell small-12 medium-10 medium-offset-1 large-8 large-offset-2">';
			} elseif( get_field('page_width', $post->ID) == 'standard' ) {
				echo '<div class="cell small-12">';
			}

			echo '<div class="entry-content wow fadeIn" itemprop="articleBody" data-wow-duration="1s">';
		}
		
	} else {
		get_template_part('templates/content-blocks/hero-blocks');
	}
}

function hover_content_acf_block_render_callback() {
	global $post;
	
	if( get_field('full_width_blocks') == 'yes' ) {
		
		if( get_field('page_width', $post->ID) == 'sidebar' ) {
			get_template_part('templates/content-blocks/hover-content-blocks');
		} else {
	
			echo '</div></div></div></div>';
				get_template_part('templates/content-blocks/hover-content-blocks');
			echo '<div class="grid-container"><div class="grid-x grid-margin-x">';

			if( get_field('page_width', $post->ID) == 'narrow' ) {
				echo '<div class="cell small-12 medium-10 medium-offset-1 large-8 large-offset-2">';
			} elseif( get_field('page_width', $post->ID) == 'standard' ) {
				echo '<div class="cell small-12">';
			}

			echo '<div class="entry-content wow fadeIn" itemprop="articleBody" data-wow-duration="1s">';
		}
		
	} else {
		get_template_part('templates/content-blocks/hover-content-blocks');
	}
}

function team_content_acf_block_render_callback() {
	global $post;
	
	if( get_field('full_width_blocks') == 'yes' ) {
		
		if( get_field('page_width', $post->ID) == 'sidebar' ) {
			get_template_part('templates/content-blocks/team-blocks');
		} else {
	
			echo '</div></div></div></div>';
				get_template_part('templates/content-blocks/team-blocks');
			echo '<div class="grid-container"><div class="grid-x grid-margin-x">';

			if( get_field('page_width', $post->ID) == 'narrow' ) {
				echo '<div class="cell small-12 medium-10 medium-offset-1 large-8 large-offset-2">';
			} elseif( get_field('page_width', $post->ID) == 'standard' ) {
				echo '<div class="cell small-12">';
			}

			echo '<div class="entry-content wow fadeIn" itemprop="articleBody" data-wow-duration="1s">';
		}
		
	} else {
		get_template_part('templates/content-blocks/team-blocks');
	}
}

function resource_content_acf_block_render_callback() {
	global $post;
	get_template_part('templates/content-blocks/resource-blocks');
}

function gallery_content_acf_block_render_callback() {
	global $post;
	
	if( get_field('full_width_blocks') == 'yes' ) {
		
		if( get_field('page_width', $post->ID) == 'sidebar' ) {
			get_template_part('templates/content-blocks/gallery-blocks');
		} else {
	
			echo '</div></div></div></div>';
				get_template_part('templates/content-blocks/gallery-blocks');
			echo '<div class="grid-container"><div class="grid-x grid-margin-x">';

			if( get_field('page_width', $post->ID) == 'narrow' ) {
				echo '<div class="cell small-12 medium-10 medium-offset-1 large-8 large-offset-2">';
			} elseif( get_field('page_width', $post->ID) == 'standard' ) {
				echo '<div class="cell small-12">';
			}

			echo '<div class="entry-content wow fadeIn" itemprop="articleBody" data-wow-duration="1s">';
		}
		
	} else {
		get_template_part('templates/content-blocks/gallery-blocks');
	}
}

?>