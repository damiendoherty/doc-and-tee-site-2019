<?php

function _wp_docandtee_create_post_types() {
	
	register_post_type( 'work',
		array(
			'labels' => array(
				'name' => __( 'Work', 'docandtee' ),
				'singular_name' => __( 'Work', 'docandtee' )
			),
			'public' => true,
			'has_archive' => false,
			'hierarchical' => false,
            'show_ui' => true,
            'show_in_rest' => true,
			'supports' => array( 'title', 'thumbnail', 'editor', 'excerpt', 'custom-fields' ),
			'rewrite' => array( 'slug' => 'work' ),
			'menu_icon' => 'dashicons-smiley'
		)
	);

	register_taxonomy(
		'worktax',
		'work',
		array(
				'hierarchical' => true,
				'label' => __( 'Work Type', 'docandtee' ),
				'show_ui' => true,
				'show_admin_column' => true,
				'rewrite' => array( 'slug' => 'work-type' )
		)
	);
    
    register_post_type( 'testimonials',
		array(
			'labels' => array(
				'name' => __( 'Testimonials', 'docandtee' ),
				'singular_name' => __( 'Testimonial', 'docandtee' )
			),
			'public' => true,
			'has_archive' => false,
			'hierarchical' => false,
			'supports' => array( 'title', 'thumbnail', 'editor', 'excerpt', 'custom-fields' ),
			'rewrite' => array( 'slug' => 'testimonial' ),
			'menu_icon' => 'dashicons-smiley'
		)
	);
    
    register_post_type( 'clients',
		array(
			'labels' => array(
				'name' => __( 'Clients', 'docandtee' ),
				'singular_name' => __( 'Client', 'docandtee' )
			),
			'public' => true,
			'has_archive' => false,
			'hierarchical' => false,
			'supports' => array( 'title', 'thumbnail', 'custom-fields' ),
			'rewrite' => array( 'slug' => 'client' ),
			'menu_icon' => 'dashicons-smiley'
		)
	);
	
	
}
add_action( 'init', '_wp_docandtee_create_post_types' );

?>
