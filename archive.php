<?php use Roots\Sage\Titles;?>

<div class="light-gray-bg archive-<?php echo get_post_type(); ?>" itemscope itemtype="http://schema.org/ItemPage">
	
	<section id="search-results-content" class="section-wrap" role="region" aria-label="Search results">
		
		<div class="grid-container">
		
			<header class="grid-x grid-margin-x wow fadeIn" data-wow-duration="1s">
				<div class="page-simple-header">
					<h1 class="entry-title"><span><?= Titles\title(); ?></span></h1>
				</div>
			</header>
			

			<?php if(have_posts()) : ?>

			<div id="search-rows" class="grid-x grid-margin-x infinitescroll">
				<?php $i=1; while (have_posts()) : the_post(); ?>
				<div id="iteration-<?php echo $i++; ?>" data-magellan-target="iteration-<?php echo $i++; ?>" class="append result-column cell small-12 medium-6 large-4 match-height">
					<?php get_template_part('templates/content-blocks/news-blocks'); ?>
				</div>
				<?php endwhile; ?>
			</div>

			<div class="grid-x grid-margin-x hide">
				<div class="cell small-12">
					<nav id="pagination" class="posts-pagination">
						<?php docandtee_numeric_posts_nav(); ?>
					</nav>
				</div>
			</div>

			<?php /*<div class="row column text-center" data-magellan>
				<a href="#iteration-1" class="button">Back to Top</a>
			</div>*/ ?>

			<?php else : ?>
			<div class="grid-x grid-margin-x"></div>
				<div class="cellsmall-12 medium-10 medium-offset-1 large-8 large-offset-2">
					<h2>Sorry no results were found</h2>
					<p>Please try another search:</p>
					 <?php get_search_form(); ?>
				</div>
			</div>

			<?php endif; ?>
		
		</div>
		
	</section>

</div><!-- .posts-wrapper -->