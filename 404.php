<?php use Roots\Sage\Titles; ?>

<section id="main-page-content" class="section-wrap">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">	
			<div class="cell small-12 medium-10 medium-offset-1 large-8 large-offset-2">

				<div class="page-simple-header">
					<h1 class="entry-title"><span><?= Titles\title(); ?></span></h1>
				</div>

				<div class="entry-content" itemprop="articleBody">
					<p>We're sorry we can't find what you're looking for.</p>
					<p>Please try another search:</p>
					<p><?php get_search_form(); ?></p>
					<p>Or check out our <a href="<?php echo esc_url( home_url( '/' ) ); ?>">homepage</a>...</p>
				</div>

			</div>
		</div>
	</div>
</section>
		