<?php if( have_rows('content_block') ): ?>
   
<section id="content-blocks" class="content-blocks simple-content-blocks grid-container fluid" aria-label="Additional page content">
	<div class="grid-x grid-margin-x">

	<?php while( have_rows('content_block') ): the_row(); ?>
	
		<?php 
		$image = get_sub_field('image_block');
		$block_title = get_sub_field('block_title');
		$block_text = get_sub_field('block_text');
		$button_text = get_sub_field('button_text');
		$button_link = get_sub_field('button_link');
		$post_thumb_sml = wp_get_attachment_image_src($image, 'post-thumb-sml');
		$post_thumb_med = wp_get_attachment_image_src($image, 'post-thumb-med');
		?>

		<div class="cell small-12 medium-auto">   
			<div class="simple-block wow fadeIn" data-wow-duration="2s">                
				<div class="simple-wrap">
					<?php if (!empty($image) ): ?>
					<?php echo wp_get_attachment_image( $image, 'large', false, array( "class" => "content-img", "data-interchange" => "[$post_thumb_sml[0], small], [$post_thumb_med[0], medium], [$post_thumb_med[0], large]")); ?>
					<?php endif; ?>

					<?php if( $block_title ) : ?>
					<header>
						<h3 class="simple-block-title" itemprop="name">
							<?php echo $block_title; ?>
						</h3>
					</header>
					<?php endif; ?>
					<?php if( $block_text ) { echo '<div class="copy-wrap">' .$block_text. '</div>'; } ?>

					<?php if( get_sub_field( 'include_button' ) ): ?>
					 <a href="<?php echo get_permalink( $button_link->ID); ?>" class="button btn-arrow cta-btn btn-secondary" role="button" aria-label="Link to read full article"><?php if( $button_text ) { echo $button_text; } ?></a>
					 <?php endif; ?>
				</div>
			</div>
		</div>


	<?php unset($image); endwhile; ?>
		
	</div>

</section>

<?php endif; ?>