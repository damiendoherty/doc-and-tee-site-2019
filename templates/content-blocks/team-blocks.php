<?php if( have_rows('team_member') ): ?>
   
<section id="content-blocks" class="content-blocks grid-container full" aria-label="Additional page content">
	<div class="grid-x grid-margin-x">

	<?php $counter=1; while( have_rows('team_member') ): the_row(); ?>
	
		<?php 
		$image = get_sub_field('team_member_photo');
		$team_member_name = get_sub_field('team_member_name');
		$team_member_bio = get_sub_field('team_member_bio');
        $team_member_role = get_sub_field('team_member_role');
        $post_thumb_sml = wp_get_attachment_image_src($image, 'medium');
		$post_thumb_med = wp_get_attachment_image_src($image, 'medium_large');
		?>
        
        <div class="cell small-12 medium-auto">
            
            <div class="team-member wow fadeIn" data-wow-duration="2s">
                <figure class="member-inner" data-open="team-modal-<?php echo $counter; ?>">

                    <?php if (!empty($image) ): ?>
                    <?php echo wp_get_attachment_image( $image, 'post-thumb-sq', false, array( "class" => "team-img",)); ?>
                    <?php endif; ?> 

                    <figcaption class="member-details">
                        <div class="details-wrap">
                            <?php if( $team_member_name ) { echo '<h4 class="member-title">' .$team_member_name. '</h4>'; } ?>
                            <?php if( $team_member_role ) { echo '<p class="member-role">' .$team_member_role. '</p>'; } ?>
                        </div>
                    </figcaption>

                </figure>
            </div>
            
	    </div>
        
        <div id="team-modal-<?php echo $counter++; ?>" class="medium reveal team-reveal" data-reveal data-animation-in="scale-in-up fast" data-animation-out="scale-out-down fast">
            <div class="reveal-inner">

                <?php if (!empty($image) ): ?>
				<?php echo wp_get_attachment_image( $image, 'large', false, array( "class" => "content-img", "data-interchange" => "[$post_thumb_sml[0], small], [$post_thumb_med[0], medium], [$post_thumb_med[0], large]")); ?>
				<?php endif; ?>

                <?php if( $team_member_name ) { echo '<h1>' .$team_member_name. '</h1>'; } ?>
                <?php if( $team_member_role ) { echo '<h3>' .$team_member_role. '</h3>'; } ?>
                <?php if( $team_member_bio ) { echo '<div class="team-bio">' .$team_member_bio. '</div>'; } ?>

            </div>

            <button class="close-button" data-close aria-label="Close" type="button" role="button">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
  

	<?php endwhile; ?>
        
    </div>

</section>

<?php endif; ?>

