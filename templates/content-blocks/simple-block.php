<article id="post-<?php the_ID(); ?>" <?php post_class('simple-block wow fadeIn'); ?> itemscope itemtype="http://schema.org/Article" data-wow-duration="2s">      
            
	<div class="simple-wrap">
		<?php if (has_post_thumbnail()) : ?>
			<a href="<?php the_permalink(); ?>" aria-label="Link to read full article" itemprop="url">
				<?php 
				$post_thumb_xsml = get_the_post_thumbnail_url($post->ID, 'post-thumb-xsml');
				$post_thumb_sml = get_the_post_thumbnail_url($post->ID, 'post-thumb-sml');
				$imgid  = get_post_thumbnail_id($post->ID);
				$imgalt = get_post_meta($imgid,'_wp_attachment_image_alt', true);
				?>
				<img data-interchange="[<?php echo $post_thumb_sml; ?>, small], [<?php echo $post_thumb_sml; ?>, medium], [<?php echo $post_thumb_xsml; ?>, large]" itemprop="image" alt="<?php echo $imgalt; ?>">
			</a>
		 <?php endif; ?>
		<header>
			<h3 class="simple-block-title" itemprop="name">
				<a href="<?php the_permalink(); ?>" aria-label="view article" itemprop="url"><?php the_title(); ?></a>
			</h3>
		</header>
		<?php if(is_single('post')) : ?>
		<?php get_template_part('templates/partials/entry-meta'); ?>
		<?php endif; ?>
	</div>
</article>