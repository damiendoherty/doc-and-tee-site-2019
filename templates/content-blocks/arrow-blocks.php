<article id="post-<?php the_ID(); ?>" <?php post_class('arrow-block wow fadeIn'); ?> itemscope itemtype="http://schema.org/Article" data-wow-duration="2s">
	 <div class="arrow-block-inner">
		 <?php if (has_post_thumbnail()) : ?>
			<div class="img-wrap">
				<?php 
				$post_thumb_sml = get_the_post_thumbnail_url($post->ID, 'post-thumb-sml');
				$post_thumb = get_the_post_thumbnail_url($post->ID, 'post-thumb');
				$imgid  = get_post_thumbnail_id($post->ID);
				$imgalt = get_post_meta($imgid,'_wp_attachment_image_alt', true);
				?>
				
				<img data-lazy="[<?php echo $post_thumb_sml; ?>, small], [<?php echo $post_thumb; ?>, medium], [<?php echo $post_thumb; ?>, large]" itemprop="image" alt="<?php echo $imgalt; ?>">
			</div>
		 <?php endif; ?>
		 <div class="content-wrap">
			<header class="arrow-block-header">
				<h3 class="arrow-block-title" itemprop="name"><?php the_title(); ?></h3>
				<a href="<?php the_permalink(); ?>" class="button big-arrow" itemprop="url"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
			</header>
		 </div>
		<a class="biglink" href="<?php the_permalink(); ?>" aria-label="Link to read full article" itemprop="url"></a>
	</div>
</article>