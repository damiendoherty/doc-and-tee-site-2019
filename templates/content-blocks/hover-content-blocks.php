<?php if( have_rows('content_block') ): ?>
   
<section class="content-blocks grid-container full bg-light-gray" aria-label="Additional page content">
    <div class="grid-x margin-x hover-wrap">
	
	<?php while( have_rows('content_block') ): the_row(); ?>
    
    <?php 
		$image = get_sub_field('image_block');
		$block_title = get_sub_field('block_title');
		$block_text = get_sub_field('block_text');
		$button_text = get_sub_field('button_text');
		$button_link = get_sub_field('button_link');
		$post_thumb_sml = wp_get_attachment_image_src($image, 'post-thumb-sml');
		$post_thumb_med = wp_get_attachment_image_src($image, 'post-thumb-med');
		?>


        <div class="cell small-12 medium-auto">   
            <article class="hover-block wow fadeIn" itemscope itemtype="http://schema.org/Article" data-wow-duration="2s">
                <div class="hover-inner-wrap">
                    <div class="hover-head">
                        <header> <?php if( $block_title ) { echo '<h3 class="hover-title">' .$block_title. '</h3>'; } ?></header>
                        <div class="hover-excerpt">
                           <?php if( $block_text ) { echo '<div class="copy-wrap">' .$block_text. '</div>'; } ?>
                             <?php if( get_sub_field( 'include_button' ) ): ?>
                             <a href="<?php echo get_permalink( $button_link->ID); ?>" class="button hover-more white-btn" role="button" aria-label="Link to read full article"><?php if( $button_text ) { echo $button_text; } ?></a>
                             <?php endif; ?>
                        </div>
                    </div>

                    <?php if (!empty($image) ): ?>
                    <div class="img-wrap">
                        <?php echo wp_get_attachment_image( $image, 'large', false, array( "class" => "content-img", "data-interchange" => "[$post_thumb_sml[0], small], [$post_thumb_med[0], medium], [$post_thumb_med[0], large]")); ?>
                        <div class="img-fade"></div>
                    </div>
                    <?php endif; ?>
                    
                     <?php if($button_link): ?>
					 <a href="<?php echo get_permalink( $button_link->ID); ?>" class="biglink" aria-label="Link to read full article"></a>
					 <?php endif; ?>

                </div>
            </article>
        </div>
        
        <?php unset($image); endwhile; ?>
        
    </div>
</section>

<?php endif; ?>