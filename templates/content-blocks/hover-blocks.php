<article id="post-<?php the_ID(); ?>" <?php post_class('hover-block wow fadeIn'); ?> itemscope itemtype="http://schema.org/Article" data-wow-duration="2s">
	<div class="hover-inner-wrap">
		<div class="hover-head">
			<header><h3 class="hover-title" itemprop="name"><?php the_title(); ?></h3></header>
			<div class="hover-excerpt">
				<?php the_excerpt(); ?>
				<a href="<?php the_permalink(); ?>" class="button hover-more white-btn" itemprop="url">Find out more</a>
			</div>
		</div>
		
		<?php if ( has_post_thumbnail() ) : ?>
		<div class="img-wrap">
			<?php 
			$post_thumb_sml = get_the_post_thumbnail_url($post->ID, 'post-thumb-sml');
			$post_thumb_med = get_the_post_thumbnail_url($post->ID, 'post-thumb-med');
			$post_thumb_lrg = get_the_post_thumbnail_url($post->ID, 'post-thumb-lrg');
			$imgid  = get_post_thumbnail_id($post->ID);
			$imgalt = get_post_meta($imgid,'_wp_attachment_image_alt', true);
			?>

			<img data-interchange="[<?php echo $post_thumb_sml; ?>, small], [<?php echo $post_thumb_med; ?>, medium], [<?php echo $post_thumb_lrg; ?>, large]" itemprop="image" alt="<?php echo $imgalt; ?>">
			<div class="img-fade"></div>
		</div>
		<?php endif; ?>

		<a class="biglink" href="<?php the_permalink(); ?>" aria-label="Link to read full article" itemprop="url"></a>
	</div>
</article>