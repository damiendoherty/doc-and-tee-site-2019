<?php if( have_rows('content_block') ): ?>
   
<section id="content-blocks" class="content-blocks grid-container full" aria-label="Additional page content">
	

	<?php while( have_rows('content_block') ): the_row(); ?>
	
		<?php 
		$image = get_sub_field('image_block');
		$block_title = get_sub_field('block_title');
		$block_text = get_sub_field('block_text');
		$button_text = get_sub_field('button_text');
		$button_link = get_sub_field('button_link');
		$post_thumb_sml = wp_get_attachment_image_src($image, 'post-thumb-sml');
		$post_thumb_med = wp_get_attachment_image_src($image, 'post-thumb-med');
		?>
	
	<div class="grid-x content-wrapper">
		
		<?php if (!empty($image) ): ?>

		<div class="content-block image-bg cell small-12 medium-6 wow fadeIn" data-wow-duration="2s">
			<div class="content-bg"><?php echo wp_get_attachment_image( $image, 'large', false, array( "class" => "content-img", "data-interchange" => "[$post_thumb_sml[0], small], [$post_thumb_med[0], medium], [$post_thumb_med[0], large]")); ?></div>
		</div>

		<?php endif; ?> 
	
		<div class="content-block cell small-12 medium-6 wow fadeIn" data-wow-duration="1s">
			 <div class="content-inner">
				 <?php if( $block_title ) { echo '<h3>' .$block_title. '</h3>'; } ?>
				 <?php if( $block_text ) { echo '<div class="copy-wrap">' .$block_text. '</div>'; } ?>
				 
					 <?php if( get_sub_field( 'include_button' ) ): ?>
					 <a href="<?php echo get_permalink( $button_link->ID); ?>" class="button btn-arrow" role="button" aria-label="Link to read full article"><?php if( $button_text ) { echo $button_text; } ?></a>
					 <?php endif; ?>
			 </div>
		</div>

	</div>

	<?php unset($image); endwhile; ?>

</section>

<?php endif; ?>

