<article id="post-<?php the_ID(); ?>" <?php post_class('news-post wow fadeIn'); ?> itemscope itemtype="http://schema.org/Article" data-wow-duration="2s">
	<div class="news-wrap">
		
		<?php if (has_post_thumbnail()) : ?>
			<a href="<?php the_permalink(); ?>" class="post-thumb-link" aria-label="Link to read full article" itemprop="url">
				<?php 
				$post_thumb_xsml = get_the_post_thumbnail_url($post->ID, 'post-thumb-xsml');
				$post_thumb_sml = get_the_post_thumbnail_url($post->ID, 'post-thumb-sml');
				$imgid  = get_post_thumbnail_id($post->ID);
				$imgalt = get_post_meta($imgid,'_wp_attachment_image_alt', true);
				?>
				
				<img data-interchange="[<?php echo $post_thumb_sml; ?>, small], [<?php echo $post_thumb_sml; ?>, medium], [<?php echo $post_thumb_xsml; ?>, large]" itemprop="image" alt="<?php echo $imgalt; ?>">
				
				<?php if ( has_post_format('video') ) {
				echo '<span class="post-format-icon"><i class="fas fa-play-circle"></i></span>'; 
				} elseif ( has_post_format('audio') ) {
				echo '<span class="post-format-icon"><i class="fas fa-volume-up"></i></span>';
				} elseif ( has_post_format('gallery') ) {
				echo '<span class="post-format-icon"><i class="fas fa-images"></i></span>';
				}?>
			</a>
		 <?php endif; ?>
		
		<div class="content-wrap">
          	<div class="content-inner">
				<?php if('post' == get_post_type()) : ?>
				<?php get_template_part('templates/partials/category-tags'); ?>
				<?php endif; ?>
				<header>
					<h3 class="news-post-title" itemprop="name"><a href="<?php the_permalink(); ?>" aria-label="Link to read full article" itemprop="url"><?php the_title(); ?></a></h3>
				</header>
				<?php if('post' == get_post_type()) : ?>
				<?php get_template_part('templates/partials/entry-meta'); ?>
				<?php endif; ?>
			</div>
			<?php the_excerpt(); ?>
		</div>
	</div>
</article>