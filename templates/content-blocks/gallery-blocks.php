<?php 
$images = get_field('custom_gallery');
$post_thumb_sml = wp_get_attachment_image_src($image, 'post-thumb-sml');
$post_thumb_med = wp_get_attachment_image_src($image, 'post-thumb-med');
if( $images ): ?>

<section id="gallery-blocks" class="gallery-blocks" aria-label="Image gallery">
    <div class="grid-container full">
        <div class="grid-x grid-margin-x isotope" data-itemselector=".box" data-colwidth=".col-width">
            <?php $counter = 1; foreach( $images as $image_id ): ?>
                <div class="cell small-12 medium-6 box block-<?php echo $counter; ?>">
                    <div class="gallery-item wow">
                        <?php echo wp_get_attachment_image( $image_id, 'large', false, array( "class" => "content-img", "data-interchange" => "[$post_thumb_sml[0], small], [$post_thumb_med[0], medium], [$post_thumb_med[0], large]")); ?>
                    </div>
                </div>
            <?php $counter++;?>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<?php endif; ?>