<?php if( have_rows('resource', $post->id) ): ?>
   
<section id="content-blocks" class="content-blocks grid-container" aria-label="Resources">
	<div class="grid-x grid-margin-x">

	<?php while( have_rows('resource') ): the_row(); ?>
	
		<?php 
		$resource_title = get_sub_field('resource_title');
		$resource_type = get_sub_field('resource_type');
		$resource_url = get_sub_field('resource_url');
        $resource_file_upload = get_sub_field('resource_file_upload');
		?>
        
        <div class="cell small-12 medium-6 large-4">
            <?php if ($resource_title): ?>
            <div class="resource">
                <?php if ($resource_type == 'url' && !empty($resource_url)) : ?>
                    <a href="<?php echo $resource_url; ?>" target="_blank" class="<?php echo $resource_type; ?>"><?php echo $resource_title; ?></a>
                <?php elseif (!empty($resource_file_upload)) : ?>
                    <a href="<?php echo $resource_file_upload; ?>" target="_blank" class="<?php echo $resource_type; ?>"><?php echo $resource_title; ?></a>
                <?php endif; ?>
            </div>
            <?php endif; ?> 
	    </div>

	<?php endwhile; ?>
        
    </div>
</section>

<?php endif; ?>

