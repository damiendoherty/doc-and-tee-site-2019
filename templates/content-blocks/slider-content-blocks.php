<?php if( have_rows('content_block') ): ?>
   
<section id="content-blocks" class="content-blocks grid-container fluid" aria-label="Additional page content">
	

	<?php while( have_rows('content_block') ): the_row(); ?>
	
		<?php 
		$slider_images = get_sub_field('slider_images');
		$block_title = get_sub_field('block_title');
		$block_text = get_sub_field('block_text');
		$button_text = get_sub_field('button_text');
		$button_link = get_sub_field('button_link');
        $button_external_link = get_sub_field('button_external_link');
		?>
	
	<div class="grid-x content-wrapper">
		
		<?php if ($slider_images): ?>

		<div class="content-block image-bg cell small-12 large-6 wow fadeIn" data-wow-duration="2s">
            <div class="orbit hideaway-surrounds-slider" role="region" data-orbit data-options="animInFromLeft:fade-in; animInFromRight:fade-in; animOutToLeft:fade-out; animOutToRight:fade-out;">

                <div class="orbit-wrapper">

                    <div class="orbit-controls">
                        <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span><i class="fas fa-chevron-left"></i></button>
                        <button class="orbit-next"><span class="show-for-sr">Next Slide</span><i class="fas fa-chevron-right"></i></button>
                    </div>

                    <ul class="orbit-container">

                        <?php $is_first=true; foreach( $slider_images as $image ): 
                        $post_thumb_sml = wp_get_attachment_image_src($image['ID'], 'post-thumb-sml');
                        $post_thumb_med = wp_get_attachment_image_src($image['ID'], 'post-thumb-med');
                        ?>
                            <li class="<?php echo ($is_first)? 'is-active':'';?> orbit-slide">
                            <figure class="orbit-figure">
                                <?php echo wp_get_attachment_image( $image['ID'], 'large', false, array( "class" => "orbit-image", "data-interchange" => "[$post_thumb_sml[0], small], [$post_thumb_med[0], medium], [$post_thumb_med[0], large]")); ?>
                                </figure>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>		</div>

		<?php $is_first=false; unset($image); endif; ?>
	
		<div class="content-block cell small-12 large-6 wow fadeIn" data-wow-duration="1s">
			 <div class="content-inner">
				 <?php if( $block_title ) { echo '<h3>' .$block_title. '</h3>'; } ?>
				 <?php if( $block_text ) { echo '<div class="copy-wrap">' .$block_text. '</div>'; } ?>
				 
					 
                     <?php if( get_sub_field( 'include_button' ) ): ?>
                         <?php if( $button_link ): ?>
					         <a href="<?php echo get_permalink( $button_link->ID); ?>" class="button btn-arrow cta-btn btn-secondary" role="button" aria-label="Link to read full article"><?php if( $button_text ) { echo $button_text; } ?></a>
                         <?php endif; ?>
                        <?php if( $button_external_link ) : ?>
                          <a href="<?php echo $button_external_link; ?>" target="_blank" class="button btn-arrow cta-btn btn-secondary" role="button" aria-label="Link to read full article"><?php if( $button_text ) { echo $button_text; } ?></a>
                        <?php endif; ?>
                     <?php endif; ?>
                    
			 </div>
		</div>

	</div>

	<?php unset($image); endwhile; ?>

</section>

<?php endif; ?>