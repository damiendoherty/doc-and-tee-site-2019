<?php if( have_rows('hero_block') ): ?>
   
<section class="hero-blocks" aria-label="Additional page content">
	

	<?php while( have_rows('hero_block') ): the_row(); ?>
	
    <?php 
    $image = get_sub_field('image_block');
    $block_title = get_sub_field('block_title');
    $block_text = get_sub_field('block_text');
    $button_text = get_sub_field('button_text');
    $button_link = get_sub_field('button_link');
    $post_thumb_sml = wp_get_attachment_image_src($image, 'post-thumb-sml');
    $post_thumb_med = wp_get_attachment_image_src($image, 'post-thumb-med');
    $post_thumb_lrg = wp_get_attachment_image_src($image, 'post-thumb-lrg');
    $post_thumb_xlrg = wp_get_attachment_image_src($image, 'post-thumb-xlrg');
    $post_thumb_xxlrg = wp_get_attachment_image_src($image, 'post-thumb-xxlrg');
    ?>
		
    <?php if (!empty($image) ): ?>

    <div class="content-bg">
        <div class="img-fade"></div>
        <?php echo wp_get_attachment_image( $image, 'large', false, array( "class" => "content-img", "data-interchange" => "[$post_thumb_sml[0], small], [$post_thumb_med[0], medium], [$post_thumb_lrg[0], large], [$post_thumb_xlrg[0], xlarge], [$post_thumb_xxlrg[0], xxlarge]")); ?>
    </div>
    

    <?php endif; ?> 
    
    <div class="hero-content-wrapper">
        <div class="grid-container">
            <div class="grid-x grid-margin-x">
                <div class="hero-content cell small-12  large-8 large-offset-2 wow fadeIn aligncenter" data-wow-duration="1s">
                 <?php if( $block_title ) { echo '<h1>' .$block_title. '</h1>'; } ?>
                 <?php if( $block_text ) { echo '<div class="copy-wrap">' .$block_text. '</div>'; } ?>

                 <?php if( get_sub_field( 'include_button' ) ): ?>
                 <a href="<?php echo get_permalink( $button_link->ID); ?>" class="button btn-lrg" role="button" aria-label="Link to read full article"><?php if( $button_text ) { echo $button_text; } ?></a>
                 <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
	

	<?php unset($image); endwhile; ?>

</section>

<?php endif; ?>

