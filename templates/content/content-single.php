<?php while (have_posts()) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope itemtype="http://schema.org/ItemPage">
		
		<?php get_template_part('templates/partials/page-header-simple'); ?>
		
		<?php if ($post->post_content == '') : ?>
		<?php else : ?>
			<section id="main-page-content" class="section-wrap" aria-label="Main page copy text">
				<?php get_template_part('templates/content/content-page'); ?>
			</section>
		<?php endif; ?>

		
	</article>
<?php endwhile; ?>

