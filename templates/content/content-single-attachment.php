<?php while (have_posts()) : the_post(); ?>
<div class="grid-container">
	<div class="grid-x grid-margin-x">
		<div class="cell small-12 medium-10 medium-offset-1 large-8 large-offset-2">
			<article id="post-<?php the_ID(); ?>" data-magellan-target="post-<?php the_ID(); ?>" <?php post_class('single-post'); ?> itemscope itemtype="http://schema.org/Article">

				<header class="post-header wow fadeIn" data-wow-duration="1s">
                    <h1 class="entry-title" itemprop="name"><?php the_title(); ?></h1>
                </header>
                
				<section id="main-post-content" class="section-wrap" aria-label="Main post copy text">
					<div class="entry-content wow fadeIn" itemprop="articleBody" data-wow-duration="1s">
                        
                        <figure class="member-inner" data-open="team-modal-<?php echo $counter; ?>">
                            <?php $image_size = apply_filters( 'wporg_attachment_size', 'large' ); echo wp_get_attachment_image( get_the_ID(), $image_size ); ?>
                            <?php if ( has_excerpt() ) : ?>
                            <figcaption class="member-details">
                                <?php the_excerpt(); ?>
                            </figcaption>
                            <?php endif; ?>
                        </figure>
                        
                        <div class="post-meta wow fadeIn" data-wow-duration="2s">
                            <?php get_template_part('templates/partials/share-buttons'); ?>
                        </div>
           
					</div>
				</section>

				<footer>
					<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
				</footer>
			</article>
		</div>
	</div>
</div>
<?php endwhile; ?>

<?php get_template_part('templates/partials/post-navigation'); ?>