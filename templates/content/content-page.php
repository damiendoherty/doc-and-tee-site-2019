<div class="grid-container">
	<div class="grid-x grid-margin-x">
		<?php if( get_field('page_width') == 'narrow' ): ?>
		<div class="cell small-12 medium-10 medium-offset-1 large-8 large-offset-2">
		<?php elseif( get_field('page_width') == 'standard' ): ?>
		<div class="cell small-12">
		<?php elseif( get_field('page_width') == 'sidebar' ): ?>
		<div class="cell small-12 medium-8 large-9">
		<?php else : ?>
		<div class="cell small-12 medium-10 medium-offset-1 large-8 large-offset-2">
		<?php endif; ?>
			<div class="entry-content wow fadeIn" itemprop="articleBody" data-wow-duration="1s"><?php the_content(); ?></div>
		</div>

		<?php if( get_field('page_width') == 'sidebar' ): ?>
		<div class="cell small-12 medium-4 large-3">
			<?php get_sidebar(); ?>
		</div>
		<?php endif; ?>

	</div>
</div>
