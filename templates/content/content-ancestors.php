<?php $kids = new WP_Query(
	array(
		'post_parent'=> $post->ID,
		'post_type' => 'page',
		'ignore_sticky_posts' => true,
		'orderby' => 'menu_order',
		'order' => 'ASC'
	)
);
if ( $kids->have_posts() ) : ?>

	<section class="ancestors section-wrap" aria-label="Child pages">
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
			<?php while ($kids->have_posts()) : $kids->the_post(); ?>
				<div class="cell small-12 medium-6 large-4">
					<?php get_template_part('templates/content-blocks/simple-block'); ?>
				</div>
			<?php endwhile; wp_reset_postdata(); ?>
			</div>
		</div>
	</section>

<?php endif; ?>