<?php /* Template Name: Custom Events Page */

global $CustomEvents; ?>

<?php while (have_posts()) : the_post(); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope itemtype="http://schema.org/ItemPage">

    <?php get_template_part('templates/partials/page-header'); ?>

    <?php if ( isset( $CustomEvents ) ) : ?>
    <?php $CustomEvents->getTemplatePart( 'search-page' ); ?>
    <?php endif; ?>

</article>
<?php endwhile; ?>
