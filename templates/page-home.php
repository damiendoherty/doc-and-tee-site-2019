<?php /* Template Name: Home Page */ ?>

<?php while (have_posts()) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope itemtype="http://schema.org/ItemPage">
	
	<section class="home-header">
        <div class="home-header-media wow slideInLeft" data-wow-duration="1.5s">
            <?php if (has_post_thumbnail()) : ?>
            <div class="img-wrap">
				<?php 
				$post_thumb_sml = get_the_post_thumbnail_url($post->ID, 'post-thumb-sml');
				$post_thumb_med = get_the_post_thumbnail_url($post->ID, 'post-thumb-med');
				$post_thumb_lrg = get_the_post_thumbnail_url($post->ID, 'post-thumb-lrg');
				$post_thumb_xlrg = get_the_post_thumbnail_url($post->ID, 'post-thumb-xlrg');
				$post_thumb_xxlrg = get_the_post_thumbnail_url($post->ID, 'post-thumb-xxlrg');
				$imgid  = get_post_thumbnail_id($post->ID);
				$imgalt = get_post_meta($imgid,'_wp_attachment_image_alt', true);
				?>
				
				<img data-interchange="[<?php echo $post_thumb_sml; ?>, small],
								[<?php echo $post_thumb_med; ?>, medium],
								[<?php echo $post_thumb_lrg; ?>, large],
								[<?php echo $post_thumb_xlrg; ?>, xlarge],
								[<?php echo $post_thumb_xxlrg; ?>, xxlarge]"
					 itemprop="image" alt="<?php echo $imgalt; ?>">
			</div>
            <?php endif; ?>
        </div>
        <div class="home-header-intro wow slideInRight" data-wow-duration="1.5s">
            <header>
                <h1 class="home-header-head"><?php the_title(); ?></h1>
            </header>
            <div class="home-header-content"><?php the_content(); ?></div>
        </div>
    </section>



    <?php get_template_part('templates/partials/home-work'); ?>

    <?php get_template_part('templates/partials/home-clients'); ?>

</article>
<?php endwhile; ?>