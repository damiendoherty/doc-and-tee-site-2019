<header id="HeaderLayered" class="layered-header show-for-large wow fadeIn" itemscope itemtype="http://schema.org/WPHeader" data-wow-duration="1s">
	
	<?php //get_template_part('templates/headers/header-woocart'); ?>
	
	<nav id="top-nav" class="top-nav" itemscope itemtype="http://schema.org/SiteNavigationElement">
						
		<div class="socials-wrap">
			<?php get_template_part('templates/partials/social-links'); ?>
		</div>

		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="button" role="button" aria-label="Login"><i class="fas fa-unlock-alt"></i> Login</a>
		<a href="#access" class="button access btn-secondary" data-access="toggle" role="button" aria-label="Access settings"><i class="fas fa-universal-access"></i> Access Settings</a>
		
		<?php get_template_part('searchform'); ?>
		
	</nav>

	<div class="grid-container header-wrap">
		<div class="grid-x grid-margin-x align-middle">
			<div class="cell shrink main-logo">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="custom-logo-link" rel="home" itemprop="url" aria-label="go to home page"><img class="svg-logo" src="<?php echo get_template_directory_uri(); ?>/dist/images/doc-tee-logo.svg" alt="<?php bloginfo( 'name' ); ?>"></a>
			</div>
			<div class="cell auto">
				<?php if (has_nav_menu('primary_navigation')) :?>
				<div id="main-menu" class="main-menu">
					<nav class="main-nav" itemscope itemtype="http://schema.org/SiteNavigationElement">
						<ul class="menu dropdown" data-dropdown-menu>
						<?php wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'container' => '', 'items_wrap' => '%3$s', 'walker' => new Roots\Sage\Extras\Foundation_Nav_Menu()]);?>
						</ul>
					</nav>
				</div>
				<?php endif;?>
			</div>
		</div>
	</div>
</header>