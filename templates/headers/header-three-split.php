<header id="HeaderThreeSplit" class="three-split-header show-for-large wow fadeIn" data-wow-duration="1s" itemscope itemtype="http://schema.org/WPHeader">
	
	<?php //get_template_part('templates/headers/header-woocart'); ?>
	
	<div class="grid-container fluid">
		<div class="grid-x grid-margin-x align-middle">

			<div class="cell shrink header-left">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="button" role="button" aria-label="Login"><i class="fas fa-unlock-alt"></i> Login</a>
			</div>

			<div class="cell auto header-center">
				<div class="grid-x grid-margin-x align-middle">

					<div class="cell auto">
						<?php if (has_nav_menu('left_navigation')) :?>
						<nav id="main-menu-left" class="main-nav left-navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
							<ul class="menu dropdown" data-dropdown-menu>
							<?php wp_nav_menu(['theme_location' => 'left_navigation', 'menu_class' => 'nav', 'container' => '', 'items_wrap' => '%3$s', 'walker' => new Roots\Sage\Extras\Foundation_Nav_Menu()]);?>
							</ul>
						</nav>
						<?php endif;?>
					</div>

					<div class="cell shrink logo-wrap">
						<div class="logo">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="custom-logo-link" rel="home" itemprop="url" aria-label="Go to home page"><img class="svg-logo main-logo" src="<?php echo get_template_directory_uri(); ?>/dist/images/doc-tee-logo.svg" alt="<?php bloginfo( 'name' ); ?>"></a>
						</div>
					</div>

					<div class="cell auto">
						<?php if ( has_nav_menu('right_navigation') ) :?>
						<nav id="main-menu-right" class="main-nav right-navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
							<ul class="menu dropdown" data-dropdown-menu>
							<?php wp_nav_menu(['theme_location' => 'right_navigation', 'menu_class' => 'nav', 'container' => '', 'items_wrap' => '%3$s', 'walker' => new Roots\Sage\Extras\Foundation_Nav_Menu()]);?>
							</ul>
						</nav>
						<?php endif;?>
					</div>

				</div>
			</div>

			<div class="cell shrink header-right">
				<?php get_template_part('templates/partials/social-links'); ?>
			</div>

		</div>
	</div>

</header>
