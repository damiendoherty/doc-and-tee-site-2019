<div id="off-canvas-menu" class="off-canvas-menu"<?php /* data-toggler=".bigup"*/ ?>>
	<div class="off-canvas-inner">
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="cell small-12 medium-12 large-8 large-offset-2">
					<?php if (has_nav_menu('primary_navigation')) :?>
					<nav class="mobile-menu" itemscope itemtype="http://schema.org/SiteNavigationElement">							
						<ul class="vertical menu accordion-menu" data-accordion-menu data-submenu-toggle="true">
							<?php wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'container' => '', 'items_wrap' => '%3$s', 'walker' => new Roots\Sage\Extras\Foundation_Nav_Menu()]);?>
						</ul>
					</nav>
					<?php endif;?>
				</div>
			</div>
		</div>
	</div>
</div>
