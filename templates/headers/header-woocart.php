<?php if ( WC()->cart->get_cart_contents_count() == 0 ) : ?>
	<?php else : ?>
	<div data-sticky-container class="sticky-header">
		<div class="sticky" data-sticky data-margin-top="0" style="width:100%;"> 
			<div class="basket-case">
				<div class="shopping-basket"><a href="<?php echo esc_url( home_url( '/basket/' ) ); ?>" aria-label="Shopping cart"><i class="fas fa-shopping-cart"></i> <?php global $woocommerce; ?><?php echo $woocommerce->cart->get_cart_total(); ?></a></div>

				<div class="user-account">
					<a href="<?php echo esc_url( home_url( '/my-account/' ) ); ?>"aria-label="My account"><i class="fas fa-user"></i>
					<?php if ( is_user_logged_in() ) : ?>
					<span class="username">
					<?php global $current_user;
							  get_currentuserinfo();
							  echo $current_user->user_firstname . "\n";
							  echo $current_user->user_lastname . "\n";
						?>
					</span>
					<?php else : ?>
					<span class="username">Guest</span>
					<?php endif; ?>
					</a>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>