<div id="access-controls" class="access-controls light-gray-bg" data-access="closed">
	<div class="grid-container">
		<div class="grid-x grid-margin-x align-center">
			<div class="inner">
				
				<a class="button btn-colour" href="<?php echo add_query_arg( array( 'style' => 'standard' ), get_permalink() ); ?>" aria-label="Colour">Colour</a>
				<a class="button btn-bw" href="<?php echo add_query_arg( array( 'style' => 'blackandwhite' ), get_permalink() ); ?>" aria-label="Black and white">Black &amp; White</a>
				<a class="button btn-hc" href="<?php echo add_query_arg( array( 'style' => 'highcontrast' ), get_permalink() ); ?>" aria-label="High contrast">High Contrast</a>
				<a class="button btn-text" href="<?php echo add_query_arg( array( 'text-only' => ( ( docandtee_is_text_only() ) ? 'no' : 'yes' ) ), get_permalink() ); ?>" aria-label="text only toggle">Text only</a>

				<div class="readable">
					Text size:
					<a href="<?php echo add_query_arg( array( 'size' => 'standard' ), get_permalink() ); ?>" class="text-size standard" aria-label="text size standard">A</a>
					<a href="<?php echo add_query_arg( array( 'size' => 'medium' ), get_permalink() ); ?>" class="text-size medium" aria-label="text size medium">A</a>
					<a href="<?php echo add_query_arg( array( 'size' => 'large' ), get_permalink() ); ?>" class="text-size large" aria-label="text size large">A</a>
					<a href="<?php echo add_query_arg( array( 'size' => 'xl' ), get_permalink() ); ?>" class="text-size xl" aria-label="text size extra large">A</a>
				</div>

				<div class="readable">
					<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'docandtee' ); ?></a>
				</div>

			</div>
		</div>
	</div>
</div>