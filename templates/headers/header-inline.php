<header id="HeaderInline" class="inline-header show-for-large wow fadeIn" data-wow-duration="1s" itemscope itemtype="http://schema.org/WPHeader">
	
	<?php //get_template_part('templates/headers/header-woocart'); ?>
	
	<div class="grid-container">
		<div class="grid-x grid-margin-x align-middle">
			
			<div class="cell shrink main-logo">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" itemprop="url" aria-label="go to home page">
					<img class="svg-logo" src="<?php echo get_template_directory_uri(); ?>/dist/images/doc-tee-logo.svg" alt="<?php bloginfo( 'name' ); ?>">
				</a>
			</div>
			
			<div class="cell auto main-menu" id="main-menu">
				<?php if (has_nav_menu('primary_navigation')) :?>
				<nav class="show-for-large main-nav" itemscope itemtype="http://schema.org/SiteNavigationElement">
					<ul class="menu dropdown" data-dropdown-menu>
					<?php wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'container' => '', 'items_wrap' => '%3$s', 'walker' => new Roots\Sage\Extras\Foundation_Nav_Menu()]);?>
					</ul>
				</nav>
				<?php endif;?>
			</div>
			
			<div class="cell shrink header-right">
				<?php get_template_part('templates/partials/social-links'); ?>
				<button type="button" class="button search-btn" data-toggle="SearchOffCanvas"><i class="fas fa-search"></i></button>
			</div>
			
		</div>
	</div>

</header>