<header id="HeaderMobile" class="mobile-header" itemscope itemtype="http://schema.org/WPHeader">

	<div data-sticky-container>
		<div class="header-wrap sticky" data-sticky data-margin-top="0" style="width:100%" data-top-anchor="0" data-sticky-on="small">

			<div class="main-logo wow bounceInDown" data-wow-duration="1.5s" data-wow-delay=".5s">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="custom-logo-link wow" rel="home" itemprop="url" aria-label="go to home page">
                    <img class="svg-logo blue" src="<?php echo get_template_directory_uri(); ?>/dist/images/docandtee-logo.svg" alt="<?php bloginfo( 'name' ); ?>">
                    <img class="svg-logo white" src="<?php echo get_template_directory_uri(); ?>/dist/images/docandtee-logo-white.svg" alt="<?php bloginfo( 'name' ); ?>">
                </a>
			</div>
			
			<?php if (has_nav_menu('primary_navigation')) :?>
			<button class="hamburger hamburger--elastic" type="button" data-toggle="bodynav" aria-label="Menu" role="button" aria-controls="navigation">
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
			</button>
			<?php endif;?>

		</div>
	</div>
	
</header>


<?php if (has_nav_menu('primary_navigation')) :?>
<div class="mobile-menu-wrapper">
	<div class="mob-inner">
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="cell small-12 medium-6 medium-offset-3">
					<nav class="mobile-menu" itemscope itemtype="http://schema.org/SiteNavigationElement">							
						<ul class="vertical menu accordion-menu" data-accordion-menu data-submenu-toggle="true">
							<?php wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'container' => '', 'items_wrap' => '%3$s', 'walker' => new Roots\Sage\Extras\Foundation_Nav_Menu()]);?>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif;?>