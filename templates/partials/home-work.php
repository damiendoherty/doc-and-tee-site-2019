<?php $the_query = new WP_Query( array(
	'post_type' => 'work',
	'posts_per_page' => 6,
	'post_status' => 'publish',
	'order' => 'ASC',
	'orderby' => 'menu_order',

) );
if ( $the_query->found_posts > 0 ) : ?>

<section class="work-grid" aria-label="Custom home content">
    <div class="grid-container full">
        <header class="grid-x grid-margin-x">
            <div class="cell small-12">
                <h1 class="entry-title" itemprop="name">Our work</h1>
            </div>
        </header>
        <div class="grid-x grid-margin-x isotope" data-itemselector=".box" data-colwidth=".col-width">
            <?php $counter = 1; while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <div class="cell small-12 medium-6 large-4 box block-<?php echo $counter; ?>">
                    <div class="work-wrap">
                        <?php get_template_part('templates/content-blocks/work-block'); ?>
                    </div>
                </div>
            <?php $counter++;?>
            <?php endwhile; ?>
        </div>
    </div>
</section>

<?php endif; wp_reset_postdata(); wp_reset_query(); ?>