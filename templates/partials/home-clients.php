<?php $the_query = new WP_Query( array(
	'post_type' => 'testimonials',
	'posts_per_page' => 6,
	'post_status' => 'publish',
	'order' => 'ASC',
	'orderby' => 'menu_order',

) );
if ( $the_query->found_posts > 0 ) : ?>

<section class="testimonials-grid secondary-bg wow slideInLeft" aria-label="Client testimonials" data-wow-duration="1.5s">
    <div class="testiwraps">
        <div class="grid-container">
            <header class="grid-x grid-margin-x">
                <div class="cell small-12">
                    <h1 class="entry-title aligncenter" itemprop="name">Kind words</h1>
                </div>
            </header>
            <div class="grid-x grid-margin-x">
                <div class="cell small-12">
                    <div class="orbit testimonials-slider" role="region" data-orbit data-options="animInFromLeft:scale-in-up; animInFromRight:scale-in-up; animOutToLeft:scale-out-down; animOutToRight:scale-out-down;">
                        <div class="orbit-wrapper">

                            <div class="orbit-controls show-for-medium">
                                <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span><i class="fas fa-chevron-left"></i></button>
                                <button class="orbit-next"><span class="show-for-sr">Next Slide</span><i class="fas fa-chevron-right"></i></button>
                            </div>

                            <ul class="orbit-container">
                            <?php $is_first=true; while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                <li class="<?php echo ($is_first)? 'is-active':'';?> orbit-slide">
                                    <blockquote class="review-quote">
                                        <?php the_content(); ?>
                                        <cite><?php the_title(); ?></cite>
                                    </blockquote>
                                </li>
                            <?php endwhile; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php endif; wp_reset_postdata(); wp_reset_query(); ?>