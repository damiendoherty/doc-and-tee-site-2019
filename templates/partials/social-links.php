<?php
$social_links = array(
    'facebook' => 'fab fa-facebook-f',
    'twitter' => 'fab fa-twitter',
    'linkedin' => 'fab fa-linkedin-in',
    'github' => 'fab fa-github',
    'instagram' => 'fab fa-instagram'
);
?>

<ul class="social-icons">
    <?php foreach ($social_links as $social_link => $social_link_icon_class) :  if (!empty(get_option($social_link . '_url'))) : ?>
        <li class="social-link-list-item">
          <a class="social-link <?php echo $social_link_icon_class ?>" href="<?php echo get_option($social_link . '_url'); ?>" target="_blank" role="button" aria-label="visit our social media account"></a>
        </li>
    <?php endif;  endforeach; ?>
</ul>