<?php
use Roots\Sage\Titles;
?>

<?php if( have_rows('slider') ): ?>
	
<div class="orbit" role="region" aria-label="<?= Titles\title(); ?>" data-orbit data-options="animInFromLeft:fade-in; animInFromRight:fade-in; animOutToLeft:fade-out; animOutToRight:fade-out;">

	<div class="orbit-wrapper">
	
		<div class="orbit-controls">
			<button class="orbit-previous"><span class="show-for-sr">Previous Slide</span><i class="fas fa-chevron-left"></i></button>
			<button class="orbit-next"><span class="show-for-sr">Next Slide</span><i class="fas fa-chevron-right"></i></button>
		</div>
		
		<ul class="orbit-container">
		
			<?php $is_first=true; while( have_rows('slider') ): the_row(); ?>
		
			<li class="<?php echo ($is_first)? 'is-active':'';?> orbit-slide">
				<figure class="orbit-figure page-header">
					
					<?php 
					$image = get_sub_field('slider_image');
					$block_title = get_sub_field('block_title');
					$block_text = get_sub_field('block_text');
					$button_text = get_sub_field('button_text');
					$button_link = get_sub_field('button_link');
					$post_thumb_sml = wp_get_attachment_image_src($image, 'post-thumb-sml');
					$post_thumb_med = wp_get_attachment_image_src($image, 'post-thumb-med');
					$post_thumb_lrg = wp_get_attachment_image_src($image, 'post-thumb-lrg');
					$post_thumb_xlrg = wp_get_attachment_image_src($image, 'post-thumb-xlrg');
					$post_thumb_xxlrg = wp_get_attachment_image_src($image, 'post-thumb-xxlrg');
					?>
					<?php if (!empty($image) ): ?>
					<div class="img-wrap">
						<?php echo wp_get_attachment_image( $image, 'large', false, array( "class" => "orbit-image", "data-interchange" => "[$post_thumb_sml[0], small], [$post_thumb_med[0], medium], [$post_thumb_lrg[0], large], [$post_thumb_xlrg[0], xlarge], [$post_thumb_xxlrg[0], xxlarge]")); ?>
					</div>
					<?php endif; ?>
					
					<?php $slider_title = get_sub_field('slider_title'); 
						$slider_call_to_action_url = get_sub_field('slider_call_to_action_url'); 
						if( $slider_call_to_action_url ) : ?>
				
					<div class="page-header-inner header-panel">

						<header class="page-header-inner-left">

							<?php if( $slider_title ) { echo '<h1 class="entry-title" itemprop="name">';
							if( $slider_call_to_action_url ) { echo '<a href="' .$slider_call_to_action_url. '" class="call-to-action-link">'; }
							echo $slider_title;
							if( $slider_call_to_action_url ) { echo '</a>'; }
							echo '</h1>'; } ?>

							<?php $slider_subtitle = get_sub_field('slider_subtitle'); if( $slider_subtitle ) { echo '<h2 class="subtitle">' .$slider_subtitle. '</h2>'; } ?>  
						</header>

						<?php $slider_call_to_action_text = get_sub_field('slider_call_to_action_text'); if( $slider_call_to_action_text ) : ?>
						<div class="page-header-inner-right">
							<div class="call-to-action">
								<?php $slider_call_to_action_url = get_sub_field('slider_call_to_action_url'); if( $slider_call_to_action_url ) { echo '<a href="' .$slider_call_to_action_url. '" class="call-to-action-link">'; } ?>
								<?php echo $slider_call_to_action_text; ?>
								<?php $slider_call_to_action_url = get_sub_field('slider_call_to_action_url'); if( $slider_call_to_action_url ) { echo '<span class="arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>'; } ?>
							</div>
						</div>
						<?php endif; ?>

					</div>
					
					<?php else : ?>

				
					<header class="page-header-inner">
						<div class="page-simple-header">
							<h1 class="entry-title" itemprop="name"><span><?php the_title(); ?></span></h1>
							<?php $slider_subtitle = get_sub_field('slider_subtitle'); if( $slider_subtitle ) { echo '<h2 class="subtitle">' .$slider_subtitle. '</h2>'; } ?>
						</div>  
					</header>
					<div class="page-header-background show-for-medium"></div>
					
					<?php endif; ?>	
	
				</figure>
			</li>
			
			<?php $is_first=false; unset($image); endwhile; ?>
			
		</ul>
		
	</div>
	
	<nav class="orbit-bullets">
		<?php $is_first=0; while( have_rows('slider') ): the_row(); ?>
		<button class="<?php echo (0==$is_first)? 'is-active':'';?>" data-slide="<?php echo $is_first++; ?>"><span class="show-for-sr">slide details.</span></button>
		<?php unset($image); endwhile; ?>
	</nav>
	
</div>


<?php endif; ?>