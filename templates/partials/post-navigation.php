<nav id="post-navigation" class="post-navigation hide" role="navigation">
	<div class="prev-post">
		<?php previous_post_link('%link', '%title'); ?>
	</div>
	<div class="next-post">
		<?php next_post_link('%link', '%title'); ?>
	</div>
</nav> <!-- end navigation -->