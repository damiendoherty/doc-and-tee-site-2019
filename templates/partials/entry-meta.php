<p class="entry-meta"><time class="updated" datetime="<?= get_post_time('c', true); ?>" itemprop="datePublished"><?= get_the_date(); ?></time><span class="author" itemprop="author"><?= __(', by', 'sage'); ?> <a href="<?= get_author_posts_url(get_the_author_meta('ID')); ?>" rel="author" class="fn"><?= get_the_author(); ?></a></span>
 </p>
