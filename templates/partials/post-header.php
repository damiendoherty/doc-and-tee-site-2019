<?php $video_embed_code = get_field('video_embed_code'); 
$audio_file = get_field('audio_file'); ?>

<header class="post-header wow fadeIn" data-wow-duration="1s">
	<?php get_template_part('templates/partials/category-tags'); ?>
	<h1 class="entry-title" itemprop="name"><?php the_title(); ?></h1>
</header>

<?php if (has_post_thumbnail()) : ?>
<div class="post-header-img wow fadeIn" data-wow-duration="2s">
	<?php if( $video_embed_code ) : ?>
	<?php echo $video_embed_code; ?>
	<?php else : ?>
	<?php 
	$post_thumb_sml = get_the_post_thumbnail_url($post->ID, 'post-thumb-sml');
	$post_thumb_med = get_the_post_thumbnail_url($post->ID, 'post-thumb-med');
	$post_thumb_lrg = get_the_post_thumbnail_url($post->ID, 'post-thumb-lrg');
	$imgid  = get_post_thumbnail_id($post->ID);
	$imgalt = get_post_meta($imgid,'_wp_attachment_image_alt', true);
	?>

	<img data-interchange="[<?php echo $post_thumb_sml; ?>, small], [<?php echo $post_thumb_med; ?>, medium], [<?php echo $post_thumb_lrg; ?>, large]" itemprop="image" alt="<?php echo $imgalt; ?>">
	<?php endif; ?>
	<?php 

if( $audio_file ) {

	$url = wp_get_attachment_url( $audio_file );
	
	?><?php echo do_shortcode('[audio src="'.$url.'"]'); ?><?php

}

?>
</div>
<?php endif; ?>

<div class="post-meta wow fadeIn" data-wow-duration="2s">
	<?php get_template_part('templates/partials/entry-meta'); ?>
	<?php get_template_part('templates/partials/share-buttons'); ?>
</div>



