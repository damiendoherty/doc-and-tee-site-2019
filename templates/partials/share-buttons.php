<div class="all-the-shares">
	
	<span class="total-shares"><i class="fas fa-share-alt"></i></span>
	
	<span class="share-buttons">
        <a class="share-btn facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode( get_permalink()); ?>" target="_blank" title="Share on Facebook" role="button" aria-label="Share this post on Facebook">
            <i class="fab fa-facebook-f"></i>
        </a>
        <a class="share-btn twitter" href="https://twitter.com/intent/tweet?text=<?php echo urlencode ( get_the_title() ); ?>&amp;url=<?php echo urlencode( get_permalink()); ?>" target="_blank" title="Share on Twitter" role="button" aria-label="Share this post on Twitter">
            <i class="fab fa-twitter"></i>
        </a>
        <a class="share-btn whatsapp" href="https://wa.me/?text=<?php echo urlencode( get_permalink()); ?>" target="_blank" title="Share on Whatsapp" role="button" aria-label="Share this post on Whatsapp">
            <i class="fab fa-whatsapp"></i>
        </a>
        <a class="share-btn email" href="mailto:?subject=<?php echo urlencode ( get_the_title() ); ?>&amp;body=<?php echo urlencode( get_permalink()); ?>" target="_blank" title="Share via email" role="button" aria-label="Send this post via email">
            <i class="fas fa-envelope"></i>
        </a>
    </span>

	
</div>