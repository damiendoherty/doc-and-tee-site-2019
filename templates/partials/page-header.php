<?php if(get_field('use_slider') ): ?>
	
	<div class="orbit-wrap wow fadeIn" data-wow-duration="2s">
		<?php get_template_part('templates/partials/page-header-orbit'); ?>
	</div>

<?php else : ?>

	<?php if (has_post_thumbnail()) : ?>
		
		<div class="page-header wow fadeIn" data-wow-duration="2s">
			
			<?php /* Regular Page header - delete if using header panel*/ ; ?>
			<header class="page-header-inner show-for-medium">
				<div class="page-simple-header">
					<h1 class="entry-title"><span><?php the_title(); ?></span></h1>
					<?php $page_header_subtitle = get_field('page_header_subtitle'); if(get_field('show_sub_title') && $page_header_subtitle ) { echo '<h2 class="subtitle">' .$page_header_subtitle. '</h2>'; } ?> 
				</div>  
			</header>
			
			<div class="page-header-background show-for-medium"></div>
			<div class="img-wrap">
				<?php 
				$post_thumb_sml = get_the_post_thumbnail_url($post->ID, 'post-thumb-sml');
				$post_thumb_med = get_the_post_thumbnail_url($post->ID, 'post-thumb-med');
				$post_thumb_lrg = get_the_post_thumbnail_url($post->ID, 'post-thumb-lrg');
				$post_thumb_xlrg = get_the_post_thumbnail_url($post->ID, 'post-thumb-xlrg');
				$post_thumb_xxlrg = get_the_post_thumbnail_url($post->ID, 'post-thumb-xxlrg');
				$imgid  = get_post_thumbnail_id($post->ID);
				$imgalt = get_post_meta($imgid,'_wp_attachment_image_alt', true);
				?>
				
				<img data-interchange="[<?php echo $post_thumb_sml; ?>, small],
								[<?php echo $post_thumb_med; ?>, medium],
								[<?php echo $post_thumb_lrg; ?>, large],
								[<?php echo $post_thumb_xlrg; ?>, xlarge],
								[<?php echo $post_thumb_xxlrg; ?>, xxlarge]"
					 itemprop="image" alt="<?php echo $imgalt; ?>">
			</div>
			
			<?php /* Regular Page header Panel - delete if using regular header */ ; ?>
			<?php /* get_template_part('templates/partials/page-header-panel'); ?>
			
			<?php /* Regular Page header - delete if using header panel*/ ; ?>
			
			<div class="grid-container hide-for-medium">
				<header class="grid-x grid-margin-x">
					<div class="page-simple-header">
						<h1 class="entry-title" itemprop="name"><span><?php the_title(); ?></span></h1>
						<?php $page_header_subtitle = get_field('page_header_subtitle'); if(get_field('show_sub_title') && $page_header_subtitle ) { echo '<h2 class="subtitle">' .$page_header_subtitle. '</h2>'; } ?> 
					</div>
				</header>
			</div>
		</div>

	<?php else : ?>
	
	<?php /* Page header - no thumbnail */ ; ?>
	<div class="grid-container">
		<header class="grid-x grid-margin-x wow fadeIn" data-wow-duration="2s">
			<div class="page-simple-header">
				<h1 class="entry-title" itemprop="name"><span><?php the_title(); ?></span></h1>
			</div>
		</header>
	</div>
          
	<?php endif; ?>
       
            
<?php endif; ?>