<div class="page-header-inner header-panel">
	<header class="page-header-inner-left">
		<h1 class="entry-title" itemprop="name"><?php the_title(); ?></h1>
		<?php $page_header_subtitle = get_field('page_header_subtitle'); if(get_field('show_sub_title') && $page_header_subtitle ) { echo '<h2 class="subtitle">' .$page_header_subtitle. '</h2>'; } ?> 
	</header>

	<?php $call_to_action_text = get_field('call_to_action_text'); if( $call_to_action_text ) : ?>
	<div class="page-header-inner-right">
		<div class="call-to-action">
			<?php $call_to_action_link_url = get_field('call_to_action_link_url'); if( $call_to_action_link_url ) : ?><a href="<?php echo $call_to_action_link_url; ?>" class="call-to-action-link" role="button" aria-label="<?php echo $call_to_action_text; ?>"><?php endif; ?>
			<?php echo $call_to_action_text; ?>
			<?php $call_to_action_link_url = get_field('call_to_action_link_url'); if( $call_to_action_link_url ) : ?><span class="arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a><?php endif; ?>
		</div>
	</div>
	<?php endif; ?>
</div>
