<?php $categories = get_the_category();
	$separator = '';
	$output = '';
	if ( ! empty( $categories ) ) {
		echo '<nav class="category-tags" role="navigation"><ul class="cat-list">';
    		foreach( $categories as $category ) {
        	$output .= '<li class="cat-tag '.$category->slug.'" itemprop="articleSection"><a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a></li>' . $separator;
    		}
    	echo trim( $output, $separator );
		echo '</ul></nav>';
	}; 
?>